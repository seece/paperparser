#!/usr/bin/python
import os
import re
import time
import datetime
import time
import argparse
import urlparse as urlparsakaali
from urlparse import urlparse 
import mako
#from collections import OrderedDict
# python 2.6 doesn't have this built-in
from OrderedDict import OrderedDict
from posixpath import basename

from mako.template import Template
from mako.lookup import TemplateLookup

def identifier_from_link(link):
	"""
	returns a dictionary key for entry map
	youtube urls are converted to plain video identifiers
	"""
	r = urlparse(link)
	if r.netloc == "youtu.be":
		ident = r.path[1:] # remove the slash from url
		return ident

	if r.netloc == "www.youtube.com":
		q = urlparsakaali.parse_qs(r.query)

		if "v" not in q:
			return link

		return q["v"][0]

	return link

def file_ext_from_link(link):
	obj = urlparse(link)
	return os.path.splitext(basename(obj.path))[1]

def cleanup_link(link):
	return re.sub(r'^.*?://', r'', link)

class Entry:
	def __init__(self, tags, links, posters, date):
		""" 
		tags: list of tags (strings)
		links: list of links (strings)
		poster: list of the poster nicknames
		date: entry date as a datetime
		"""

		self.tags = tags
		self.links = links
		self.date = date
		self.posters = posters

	def __str__(self):
		return "%s %s %s" % (self.tags, self.links, self.date)

	def __repr__(self):
		return str(self)

	def getlink(self):

		return self.links[0]

	def haslink(self):
		if len(links) == 0:
			return False

		if links[0].strip() == "":
			return False

		return True

parser = argparse.ArgumentParser(description='Process some logs.')
parser.add_argument('logfile')
parser.add_argument('htmlfile')

args = parser.parse_args()

# pat = re.compile(r'\d\d:\d\d <[ @+](*.?)>')
date_pat = re.compile(r'--- Day changed (.*)')
pat = re.compile(r'\d\d:\d\d <[ @+](.*?)> (.*)')
keyword_pat = re.compile(r'!([A-Za-z0-9_]*?)\s(.*)')
url_pat = re.compile(r'(http|https|ftp|gopher)://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+')

first_date = None
current_date = None
aliases = {
		'p' : 'paper',
		'l' : 'link',
		'ref' : 'reference'
		}
entries = {}
starttime = time.time()

logf = open(args.logfile)
for line in logf:
	line = unicode(line.decode('utf8'))

	date_res = date_pat.match(line)

	if date_res:
		current_date = datetime.datetime.strptime(date_res.group(1), '%a %b %d %Y')
		if first_date is None:
			first_date = current_date # :)

		continue

	res = pat.match(line)

	if not res:
		continue

	# print res.group(2)
	papermatch = keyword_pat.match(res.group(2))

	if not papermatch:
		continue

	group = papermatch.group(1)

	if group == "":
		continue

	if group in aliases:
		group = aliases[group]

	if group not in entries:
		entries[group] = OrderedDict()

	words = papermatch.group(2).split(" ")
	links = []
	tags = []

	for word in words:
		if url_pat.match(word):
			links.append(word)
		else:
			tags.append(word)

	nick = res.group(1)
	entry = Entry(tags, links, [nick], current_date)

	if not entry.haslink():
		continue;

	# join tags and nicks if already exists
	ident = identifier_from_link(entry.getlink())
	if ident in entries[group]:
		old_entry = entries[group][ident]
		old_entry.tags += entry.tags
		
		if nick not in old_entry.posters:
			old_entry.posters.append(nick)	
	else:
		entries[group][ident] = entry

logf.close()

total = time.time() - starttime

total_entries = reduce(lambda x, y: x + len(y), entries, 0)
duration = (current_date - first_date).days
print "Log duration: %s days" % (duration)
print "Found %s entries in %s categories." % (total_entries, len(entries))
print "Parsing took %s s" % (total)

template = Template(
	filename="template.html", 
	default_filters=['decode.utf8'],
	input_encoding='utf-8',
	output_encoding='utf-8'	
	)
html = template.render(
	entries=entries,
	filename=args.logfile,
	cleanup_link=cleanup_link,
	aliases=aliases,
	file_ext=file_ext_from_link,
	today=datetime.datetime.now()
	)

outfile = open(args.htmlfile, 'w')
outfile.write(html)
outfile.close()
