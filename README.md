# paperparser

Extracts interesting links from IRC-logs.

## Requirements
Python 2.7 and Mako.

### Dependencies
You can install mako with pip:

	pip install mako


## Usage

	usage: papers.py [-h] logfile htmlfile

The output is a nice html list saved in `htmlfile`.
